import django
from rest_framework.generics import ListAPIView
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.filters import SearchFilter
from .models import Books,Auther,Category
from .serializers import CategorySerializer,BooksSerializer,AutherSerializer
from django.db.models import Max

    
class AddAuther(APIView):
    def post(self, request):
        serializer = AutherSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    def get(self, request):
        category= Auther.objects.all()
        serializer = AutherSerializer(category, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

class AddBook(APIView):
    def post(self, request):
        serializer = BooksSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)    

class AddCategory(APIView):
    def post(self, request):
        serializer = CategorySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)  
    
    def get(self, request):
        category= Category.objects.all()
        serializer = AutherSerializer(category, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
          
class MostBookSoldByAuther(APIView):
    def get(self, request,pk):
    
        category= Books.objects.filter(auther_id=pk).aggregate(Max('sold_count'))
        # serializer = BooksSerializer(category,many=True)
        return Response(category, status=status.HTTP_200_OK)

        
class MostBookSoldByCategory(APIView):
    def get(self, request,pk):
        category= Books.objects.filter(category_id=pk).aggregate(Max('sold_count'))
        # serializer = BooksSerializer(category,many=True)
        return Response(category, status=status.HTTP_200_OK)
    
class SearchBook(APIView):
    def get(self, request):
        queryset= Books.objects.filter(name__icontains= request.GET.get('q'))
        serializer = BooksSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

class GetBookByAuther(APIView):
    def get(self,request,pk):
        try:
            category= Books.objects.get(id=pk)
            serializer = BooksSerializer(category)
            return Response(serializer.data, status=status.HTTP_200_OK)
        except Books.DoesNotExis:
            return Response({"message": "book not exist"},status=status.HTTP_400_BAD_REQUEST)