from rest_framework import serializers
from .models import Books,Auther,Category

class BooksSerializer(serializers.ModelSerializer):
    class Meta:
        model = Books
        fields = '__all__'

class AutherSerializer(serializers.ModelSerializer):
    class Meta:
        model = Auther
        fields = ('name', 'phone_number', 'birth_date', 'death_date')

class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'
