from django.db import models
class Auther(models.Model):
    name=models.CharField(max_length=100)
    phone_number=models.IntegerField()
    birth_date=models.DateField()
    death_date=models.DateField()

class Category(models.Model):
    name=models.CharField(max_length=100)
    
class Books(models.Model):
    name=models.CharField(max_length=100)
    sold_count=models.IntegerField()
    price=models.IntegerField()
    publisher=models.CharField(max_length=100)
    publish_date=models.DateField()
    auther_id = models.ForeignKey(Auther, on_delete=models.CASCADE,related_name='auther')
    category_id = models.ForeignKey(Category, on_delete=models.CASCADE)
