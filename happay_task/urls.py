from django.contrib import admin
from django.urls import path
from app import  views
urlpatterns = [
    path('admin/', admin.site.urls),
    path('addauther/', views.AddAuther.as_view()),
    path('addbook/', views.AddBook.as_view()),
    path('addcategory/', views.AddCategory.as_view()),
    path('mostbooksoldauther/<pk>/', views.MostBookSoldByAuther.as_view()),
    path('mostbooksoldcategory/<pk>/', views.MostBookSoldByCategory.as_view()),
    path('searchbook/', views.SearchBook.as_view()),
    path('getbookbyauther/<pk>/', views.GetBookByAuther.as_view()),

]
